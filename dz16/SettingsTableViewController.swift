//
//  SettingsTableViewController.swift
//  dz16
//
//  Created by Даниил Карпитский on 2/14/22.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    // MARK: keys for userDefaults
    let kSettingsFirstName:String =     "First name"
    let kSettingsLastName:String =      "Last name"
    let kSettingsLogin:String =         "Login"
    let kSettingsPassword:String =      "Password"
    let kSettingsAge:String =           "Age"
    let kSettingsPhoneNumber:String =   "Phone Number"
    let kSettingsEmail:String =         "Email"
    let kSettingsAdress:String =        "Adress"
    let kSettingsPaymentMethod:String = "Payment Method"
    let kSettingsTips:String =          "Tips"
    
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var ageField: UITextField!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var adressField: UITextField!
    @IBOutlet weak var paymentMethodControl: UISegmentedControl!
    @IBOutlet weak var switchControl: UISwitch!
    
    

    @IBAction func clearButtonPressed(_ sender: Any) {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        loadSettings()
    }
    
    @IBAction func autoSave(_ sender: Any) {
        saveSettings()
    }
    
    
    @IBAction func donePressed(_ sender: UITextField) {
        sender.resignFirstResponder()
//        saveSettings()
    }

   
    override func viewDidLoad() {
        super.viewDidLoad()
        loadSettings()
    }
    
    //MARK: userDefaults load and set
    //load settings to the textfields
    func loadSettings(){
        let  defaults = UserDefaults.standard
        firstNameField.text = defaults.string(forKey: kSettingsFirstName)
        lastNameField.text = defaults.string(forKey: kSettingsLastName)
        loginField.text = defaults.string(forKey: kSettingsLogin)
        passwordField.text = defaults.string(forKey: kSettingsPassword)
        ageField.text = defaults.string(forKey: kSettingsAge)
        phoneNumberField.text = defaults.string(forKey: kSettingsPhoneNumber)
        emailField.text = defaults.string(forKey: kSettingsEmail)
        adressField.text = defaults.string(forKey: kSettingsAdress)
        paymentMethodControl.selectedSegmentIndex = defaults.integer(forKey: kSettingsPaymentMethod)
        switchControl.isOn = defaults.bool(forKey: kSettingsTips)
    }
    //save settings from the textFields
    func saveSettings() {
        let defaults = UserDefaults.standard
        defaults.set(firstNameField.text, forKey: kSettingsFirstName)
        defaults.set(lastNameField.text, forKey: kSettingsLastName)
        defaults.set(loginField.text, forKey: kSettingsLogin)
        defaults.set(passwordField.text, forKey: kSettingsPassword)
        defaults.set(ageField.text, forKey: kSettingsAge)
        defaults.set(phoneNumberField.text, forKey: kSettingsPhoneNumber)
        defaults.set(emailField.text, forKey: kSettingsEmail)
        defaults.set(adressField.text, forKey: kSettingsAdress)
        defaults.set(paymentMethodControl.selectedSegmentIndex, forKey: kSettingsPaymentMethod)
        defaults.set(switchControl.isOn, forKey: kSettingsTips)
    }
    
    

}
